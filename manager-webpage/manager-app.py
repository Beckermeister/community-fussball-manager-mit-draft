#!/usr/bin/env python

from datetime import datetime
import flask, psycopg2
from flask import sessions
import models.player as player
import os
import hashlib
import hmac
import time
import config

# TODO: Session für eingeloggten user erstellen, sodass er auf der Website eingeloggt bleibt
app = flask.Flask(__name__)

cfg = config.Config()
conn = psycopg2.connect(
    host= cfg.host,
    database=cfg.database,
    user=cfg.user,
    password=cfg.password
)

cur = conn.cursor()
app.secret_key = cfg.secret

cur.execute("SELECT version()")
db_version = cur.fetchone()
print(db_version)

@app.route('/', methods = ['GET'])
def home():
    return flask.render_template('home.html')

@app.route('/home/<int:num>', methods = ['GET'])
def disp(num):

    return flask.jsonify({'data': num**2})

# User Signup und Login
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@app.route('/signup', methods = ['GET', 'POST'])
def signup():
    req = flask.request
    if req.method == 'POST':
        username = req.form['username']
        password = req.form['password']

        cur.execute("""SELECT EXISTS(SELECT * FROM  users WHERE username=%s);""", (username, ))
        user_exists = cur.fetchone()[0]
        if user_exists:
            return flask.render_template('user_create.html', message="Benutzername existiert bereits")
        
        salt = os.urandom(16)
        pw_hash = hashlib.pbkdf2_hmac('sha512', password.encode(), salt.hex().encode('utf-8'), 200000)
        db_pw = "%s:%s" % (salt.hex(), pw_hash.hex())
        now = datetime.now()

        cur.execute("""INSERT INTO users(username, password, created_at)
            VALUES(%s, %s, %s);""", (username, db_pw, str(now)))
        conn.commit()
        
        return flask.redirect("signup_success")
    
    return flask.render_template('user_create.html')

@app.route('/signup_success', methods=['GET'])
def signup_success():
    return flask.render_template("login.html", message = "Benutzer Account erstellt")

@app.route('/login', methods = ['GET', 'POST'])
def login():
    if 'username' not in flask.session:
        req = flask.request
        if req.method == 'POST':
            username = req.form['username']
            password = req.form['password']

            cur.execute("""SELECT password, ID FROM users WHERE username = %s""", (username, ))
            user_data = cur.fetchone()
            stored_pw = user_data[0]
            user_id = user_data[1]

            salt = stored_pw.split(':')[0].encode('utf-8')
            stored_pw = stored_pw.split(':')[1].encode('utf-8')
            pw_correct = hmac.compare_digest(
                stored_pw,
                hashlib.pbkdf2_hmac('sha512', password.encode(), salt, 200000).hex().encode('utf-8')
            )
            if pw_correct:
                flask.session['username'] = username
                flask.session['id'] = user_id
                return flask.render_template('home.html', message = "Willkommen %s" % username)
            else:
                return flask.render_template('login.html', message = "Login failed")
    
        return flask.render_template('login.html')
    else:
        return flask.redirect('already_logged_in')

@app.route('/not_logged_in', methods = ['GET'])
def not_logged_in():
    return flask.render_template('not_logged_in.html')

@app.route('/logged_in', methods=['GET'])
def already_logged_in():
    if 'username' in flask.session:
        return flask.render_template('logged_in.html')
    else:
        return flask.redirect('not_logged_in')

@app.route('/logout', methods=['GET'])
def logout():
    flask.session.pop('username', None)
    flask.session.pop('id', None)
    return flask.redirect(flask.url_for('home'))

# Alle Routen die mit der Verwaltung der (Fussball-)Spieler zusammenhängen
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@app.route('/players', methods = ['GET'])
def display_players():
    if 'username' in flask.session:
        query = """SELECT * FROM players"""
        cur.execute(query)
        result = cur.fetchall()
        return flask.render_template('players.html', players=result)
    else:
        return flask.redirect('not_logged_in')


@app.route('/players/create', methods = ['GET', 'POST'])
def create_player():
    if 'username' in flask.session:
        if flask.request.method == 'POST':
            cur.execute("""INSERT INTO players(first_name, last_name, salary) VALUES(%s, %s, %s) RETURNING ID;""", (flask.request.form['firstName'], flask.request.form['lastName'], flask.request.form['salary']))
            id = cur.fetchone()
            return flask.redirect('/players/%i' % id[0])
        return flask.render_template('player_create.html')
    else:
        return flask.redirect('not_logged_in')

@app.route('/players/<int:id>', methods = ['GET'])
def display_player(id):
    
    cur.execute("""SELECT * FROM players WHERE ID = %s""", (str(id), ))
    result = cur.fetchone()
    return flask.render_template('player.html', player=result)

# Alle Routen die mit der Verwaltung der Spielrunden zusammenhängen
# --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
@app.route('/games', methods = ['GET'])
def display_games():
    if 'username' in flask.session:
        query = """SELECT * FROM games"""
        cur.execute(query)
        result = cur.fetchall()
        return flask.render_template('game/games.html', games=result)
    else:
        return flask.redirect('not_logged_in')
    

@app.route('/games/create', methods = ['GET', 'POST'])
def create_game():
    if 'username' in flask.session and flask.request.method == 'POST':
        cur.execute("""INSERT INTO games(game_name, player_count)
                VALUES(%s, %s) RETURNING ID;
                """, (flask.request.form['gameName'], str(1)))
        game_id = cur.fetchone()[0]
        cur.execute("""INSERT INTO user_games(user_id, games_id) VALUES(%s, %s)""", (flask.session['id'], game_id))
        conn.commit()
        return flask.redirect('/games/%i' % game_id)
    elif 'username' in flask.session:
        return flask.render_template('game/game_create.html')
    else:
        return flask.redirect('not_logged_in')

@app.route('/games/<int:id>', methods = ['GET'])
def display_game(id):
    if 'username' in flask.session:
        cur.execute("""SELECT user_games.user_id, user_games.games_id, users.username, games.game_name, games.player_count FROM user_games LEFT JOIN users ON user_games.user_id = users.ID LEFT JOIN games ON user_games.games_id = games.ID WHERE user_games.games_id = %s;""", (str(id), ))
        result = cur.fetchall()
        user_in_game = False
        for row in result:
            if flask.session['username'] == row[2]:
                user_in_game = True
                break

        return flask.render_template('game/game.html', games=result, user_in_game=user_in_game)
    else:
        return flask.redirect('not_logged_in')

@app.route('/games/join/<int:id>', methods = ['GET', 'POST'])
def join_game(id):
    req = flask.request
    if req.method=='GET':
        cur.execute("""SELECT game_name, ID FROM games WHERE ID = %s""", (str(id), ))
        result = cur.fetchone()
        return flask.render_template('game/game_join.html', user_id = flask.session['id'], game_name=result[0], game_id=result[1], success=False)

    if req.method == 'POST':
        if 'username' in flask.session:
            cur.execute("""SELECT user_games.user_id, user_games.games_id, users.username, games.game_name, games.player_count FROM user_games LEFT JOIN users ON user_games.user_id = users.ID LEFT JOIN games ON user_games.games_id = games.ID WHERE user_games.games_id = %s;""", (str(id), ))
            result = cur.fetchall()
            user_in_game = False
            for row in result:
                if flask.session['username'] == row[2]:
                    user_in_game = True
                    break
                
            if user_in_game:
                return flask.render_template('game/game_join.html', message="Schon dem Spiel beigetreten")

            if req.form['game_id'] == str(id):
                cur.execute("""INSERT INTO user_games(user_id, games_id) VALUES (%s, %s)""", (flask.session['id'], str(id)))
                conn.commit()
                return flask.render_template('game/game_join.html', game_id=str(id), game_name=req.form['game_name'], success=True)
            return "neinnein, böser junge"
        else:
            return flask.redirect('not_logged_in')

@app.route('/games/<int:id>/leave', methods=['GET'])
def leave_game(id):
    if 'username' in flask.session:
        cur.execute("""DELETE FROM user_games WHERE user_id = %s AND games_id = %s""", (flask.session['id'], str(id)))
        cur.execute("""DELETE FROM user_games_players WHERE user_id = %s AND game_id = %s""", (flask.session['id'], str(id)))
        return flask.redirect(flask.url_for('display_game', id=id))
    return flask.redirect('not_logged_in')


if __name__ == '__main__':
    app.run(debug=True)
    conn.close()
