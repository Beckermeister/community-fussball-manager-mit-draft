class Player:
    def __init__(self, first_name, last_name, position, grade):
        self.first_name = first_name
        self.last_name = last_name
        self.position = position
        self.grade = grade

    def to_string(self):
        return f"{self.first_name} {self.last_name}, {self.position}: {self.grade}"

    def to_json(self):
        return { 'first_name': self.first_name, 'last_name': self.last_name, 'position': self.position, 'grade': self.grade }

