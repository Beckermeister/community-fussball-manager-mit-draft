import player


class Team:
    def __init__(self, name, players):
        self.name = name
        self.players = players

    def to_string(self):
        ausgabe = f"{self.name}\n"
        for spieler in self.players:
            ausgabe.join(spieler.to_string)

        return ausgabe

