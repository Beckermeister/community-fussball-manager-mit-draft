class Player:
    def __int__(self, first_name, last_name, position, grade):
        self.first_name = first_name
        self.last_name = last_name
        self.position = position
        self.grade = grade

    def to_string(self):
        return f"{self.first_name} {self.last_name}, {self.position}: {self.grade}"

