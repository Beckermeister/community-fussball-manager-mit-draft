import urllib.request
import re
import team
import player
import datetime
import time


class KickerPlayerRatings:

    def get_team_ratings_round(self, league, team_name, season, round):
        # TODO regex page, safe info, output info
        # Takes the season format as follows: 202x-2x+1, round is 1-34
        url = f"https://www.kicker.de/{league}/{team_name}/topspieler-spieltag/{season}/{round}"
        page = urllib.request.urlopen(url)
        site = page.read()
        site = site.decode("utf-8")

        # Regex cleanup for the table containing the Information
        # Table starts at first class "sportItemEven", ends at the first </td> after the last sportItemEven \w/:.\s<>
        pattern = r'kick__table--ranking__playername[\w\W]+kick__pagination kick__card--padding'
        # pattern = r'<td class="sportItemEven" align="center">[\w\W]+</nobr>'
        contents = re.findall(pattern=pattern, string=site)
        content = contents[0]
        del contents

        # seperates the data from the website into the single players
        pos = content.find(r'</span></td>')
        contents = []
        while pos != -1:
            contents.append(content[0:pos + 10])
            content = content[pos + 7:-1]
            pos = content.find(r'</span></td>')

        for i in range(len(contents)):
            pos1 = contents[i].find(team_name)
            pos2 = contents[i].find(r'</span></td')
            contents[i] = contents[i][pos1:pos2].strip()

        for i in range(len(contents)):
            pic_pos = contents[i].find(r'<picture class=')
            if pic_pos is not -1:
                end_pos = contents[i].find(r'</a>')
                contents[i] = contents[i][end_pos+4:-1].strip()

        players = []
        for content in contents:
            lname_beg = content.find(r'<span>') + len(r'<span>')
            lname_end = content.find(r'</span>')
            fname_end = content.find(r'</a>')
            grade_identifier = r'ranking__mark"><span>'
            grade_beg = content.find(grade_identifier) + len(grade_identifier)
            grade_end = content.find(r'<', grade_beg)

            first_name = content[lname_end + len(r'</span>'):fname_end].strip()
            last_name = content[lname_beg:lname_end].strip()
            grade = content[grade_beg:grade_end]
            players.append([first_name, last_name, grade])
        for play in players:
            print(play)


if __name__ == "__main__":
    worker = KickerPlayerRatings
    ligen = ["bundesliga",
             "2-bundesliga"]
    teams_erste_liga = ["fc-augsburg",
                "fc-bayern-muenchen"]
    teams_zweite_liga = ["erzgebirge-aue",
                "vfl-bochum",
                "eintracht-braunschweig",
                "sv-darmstadt-98",
                "fortuna-duesseldorf",
                "spvgg-greuther-fuert",
                "hannover-96",
                "1-fc-heidenheim",
                "hamburger-sv",
                "karlsruher-sc",
                "holstein-kiel",
                "1-fc-nuernberg",
                "vfl-osnabrueck",
                "sc-paderborn-07",
                "jahn-regensburg",
                "sv-sandhausen",
                "fc-st-pauli",
                "wuerzburger-kickers"]
    for team in teams_zweite_liga:
        worker.get_team_ratings_round(worker, ligen[1], team, "2020-21", "1")
        time.sleep(3)

