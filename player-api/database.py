import psycopg2
import models.player as player


def initialize_db():
    commands = (
        """
        CREATE TABLE IF NOT EXISTS games (
            ID SERIAL PRIMARY KEY,
            game_name VARCHAR(255) NOT NULL,
            player_count int8 NOT NULL
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS players(
            ID SERIAL PRIMARY KEY,
            first_name VARCHAR(255) NOT NULL,
            last_name VARCHAR(255),
            salary BIGINT
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS users(
            ID SERIAL PRIMARY KEY,
            username VARCHAR(255) NOT NULL,
            password VARCHAR(512),
            created_at DATE
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS user_games(
            user_id INT,
            games_id INT,
            CONSTRAINT fk_user_id
                FOREIGN KEY(user_id)
                    REFERENCES users(ID),
            CONSTRAINT fk_games_id
                FOREIGN KEY(games_id)
                    REFERENCES games(ID)
        )
        """,
        """
        CREATE TABLE IF NOT EXISTS user_games_players(
            user_id INT,
            game_id INT,
            player_id INT,
            position INT,
            CONSTRAINT fk_user_id
                FOREIGN KEY(user_id)
                    REFERENCES users(ID),
            CONSTRAINT fk_games_id
                FOREIGN KEY(game_id)
                    REFERENCES games(ID),
            CONSTRAINT fk_player_id
                FOREIGN KEY(player_id)
                    REFERENCES players(ID) 
        )
        """

    )


    conn = psycopg2.connect(
        host="localhost",
        database="players",
        user="postgres",
        password="Ichhass3"
    )

    cur = conn.cursor()
    for command in commands:
        cur.execute(command)
        conn.commit()
    conn.close()

if __name__ == "__main__":
    initialize_db()
