#!/usr/bin/env python

import flask, psycopg2
import models.player as player

app = flask.Flask(__name__)


conn = psycopg2.connect(
    host="localhost",
    database="players",
    user="postgres",
    password=""
)

cur = conn.cursor()

cur.execute("SELECT version()")
db_version = cur.fetchone()
print(db_version)

@app.route('/', methods = ['GET', 'POST'])
def home():
    if(flask.request.method == 'GET'):
        data = "hello world"
        return flask.jsonify({'data': data})

@app.route('/home/<int:num>', methods = ['GET'])
def disp(num):

    return flask.jsonify({'data': num**2})

@app.route('/player', methods = ['GET'])
def display():

    return flask.jsonify(player1=test_player.to_json())

@app.route('/games', methods = ['GET'])
def display_games():

    query = """SELECT * FROM games"""
    cur.execute(query)
    result = cur.fetchone()
    return flask.jsonify({'games': result})


if __name__ == '__main__':
    app.run(debug=True)
    conn.close()